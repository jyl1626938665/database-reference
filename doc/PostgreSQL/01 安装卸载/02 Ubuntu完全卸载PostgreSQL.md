# 1 删除相关的安装
```
sudo apt-get --purge remove postgresql\*
```

# 2 查找依赖包
```
dpkg -l | grep postgres
```

# 3 删除依赖包
```
apt-get --purge remove package1 package2 ...
```

# 4 删除配置及相关文件
```
sudo rm -r /etc/postgresql/
sudo rm -r /etc/postgresql-common/
sudo rm -r /var/lib/postgresql/
```

# 5 删除用户和所在组
```
sudo userdel -r postgres
sudo groupdel postgres
```