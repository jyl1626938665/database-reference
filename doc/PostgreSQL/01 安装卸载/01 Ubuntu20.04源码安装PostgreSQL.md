1 下载源码文件，本示例使用源码安装包：`postgresql-14.2.tar.bz2`（[下载地址](https://www.postgresql.org/ftp/source/)）。

2 上传到特定目录后解压，解压缩命令：`tar -jxvf postgresql-14.2.tar.bz2`。

3 进入解压后目录`postgresql-14.2`。

4 执行命令：`./configure --prefix=/usr/local/pgsql`