# MySQL配置方法
MySQL有几种指定配置（Options）的方法：
- 在命令行程序名称后配置
- 在文件中配置，当MySQL程序启动时读取此文件中的配置信息
- 配置在环境变量中

以上三种配置方式最常见的是使用配置文件（第二种）。MySQL在Windows操作系统下的配置文件是`my.ini`，位于MySQL安装目录的根目录下，在Linux操作系统下的配置文件是`/etc/my.cnf`、`/etc/mysql/my.cnf`。


# 配置生效顺序
配置按顺序生效，如果一项配置被指定多次，那么生效的是最后一次指定（有一个例外：`mysqld`的`--user`）。

配置文件在Unix和类Unix系统中的读取顺序
| 文件名称 | 目的 |
|---|---|
| /etc/my.cnf | 全局配置选项 |
| /etc/mysql/my.cnf | 全局配置选项 |
| SYSCONFDIF/my.cnf | 全局配置选项 |
| $MYSQL_HOME/my.cnf | 服务器指定配置选项（仅针对服务器） |
| defaults-extra-file | 用`--defaults-extra-file`指定的文件 |
| ~/.my.cnf | 用户指定配置选项 |
| ~/.mylogin.cnf | 用户指定登录路径配置选项（仅针对客户端） |
| DATADIR/mysqld-auto.cnf | 使用`SET PERSIST`或`SET PERSIST_ONLY`持久化的系统变量（仅针对服务器） |


# 配置项说明
## [client]
- port<br>
  客户端端口号，示例：`port=3306`

- socket

- default_character_set<br>
  客户端字符集，示例：`default_character_set=utf8`

## [mysqld]


- back_log<br>
  监听队列中可以保持的连接数，对于没建立TCP连接的请求放入监听队列，队列大小为`back_log`，如果数据库存在非常多的连接且出现`connection refused`错误，就应该增大`back_log`的值。可以检查操作系统文档获取这个变量的最大值，如果将此值设置得比操作系统限制的值更大将无效果。

- basedir<br>

- bind-address<br>

- binlog_cache_size



- connect-timeout<br>
  连接超时前的最大秒数，Linux平台上也用作等待服务器首次回应的时间。

- datadir<br>







- expire_logs_days

- general_log

- general_log_file

- interactive-timeout<br>
  关闭连接前允许`interactive-timeout`秒（取代`wait_timeout`）的不活动时间。客户端的会话`wait_timeout`变量被设为会话`interactive_timeout`变量的值。如果前端程序采用短连接，建议缩短这2个值，如果前端程序采用长连接可以直接注释掉这两个选项。

- log-bin<br>

- log-bin-index

- log_slave_updates

- log-error

- log_long_format

- log_output

- log-queries-not-using-indexes

- log-slow-admin-statements

- log_slow_queries

- log-slow-slave-statements

- log-warnings

- long-query-time

- long-slow-admin-statements

- max_allowed_packet<br>

- max_binlog_size

- max_connections<br>
  指定MySQL允许的最大连接进程数。如果访问数据库时出现`Too Many Connections`错误，需要增大此参数值。

- max_connect_errors<br>
  如果某个用户发起的连接error超过此数值，则该用户的下次连接将被阻塞，直至管理员执行`flush hosts`命令或服务重启，防止非法密码或其他原因的入侵。

- max_heap_table_size<br>

- max_relay_log_size

- min_examined_row_limit<br>

- net_buffer_length<br>

- net_read_timeout<br>

- net_retry_count<br>

- net_write_timeout<br>

- open_files_limit<br>
  MySQL打开的文件描述符限制，默认最小1024。当没有设置`open_files_limit`时，取`max_connections*5`和`ulimit -n`中的大值；当没有设置`open_files_limit`时，取`open_files_limit`和`max_connections*5`中的大值。

- pid-file

- port<br>
  监听TCP/IP连接请求的端口号。

- query_cache_limit<br>

- query_cache_min_res_unit<br>

- query_cache_size<br>

- relay-log<br>

- relay_log_index

- relay-log-purge

- replicate-wild-ignore-table

- server-id<br>

- skip-external-locking<br>
  不适用系统锁定。

- skip-name-resolve<br>
  进制MySQL对外部连接进行DNS解析，使用这一选项可以节省MySQL进行DNS解析的时间，如果开启此选项，所有远程主机都只能使用IP进行连接。

- skip-networking<br>
  开启该选项可以彻底关闭MySQL的TCP/IP连接方式，如果Web服务器以远程方式访问MySQL数据库则不要开启该选项，否则将无法正常连接。如果所有进程都在同一台服务器连接到本地的mysqld，这样设置会更安全。

- skip-slave-start<br>
  启动MySQL但不启动复制。

- skip-symbolic-links<br>
  不能使用连接文件，多个客户可能访问同一个数据库，此配置防止外部客户锁定MySQL服务器。该选项默认开启。

- slave-load-temdir<br>

- slave-net-timeout<br>

- slave_skip_errors

- slow_query_log<br>

- slow_query_log_file<br>

- socket<br>

- sysdate-is-now<br>

- table_cache<br>

- tempdir<br>

- tmp_table_size

- thread_cache_size

- thread_concurrency

- thread_stack<br>

- wait-timeout<br>
  等待关闭连接的时间。










## 服务器端工具
```
# 服务器端工具，用于启动mysqld，也是mysqld的守护进程，当mysql被kill时，mysqld_safe负责重启
[mysqld_safe]
# MySQL打开的文件描述符限制，是MySQL中的一个全局变量且不可动态修改，控制着mysqld进程能使用的最大文件描述符数量，默认最小值为1024
# 需要注意的是这个变量的值不一定是此处设置的值，mysqld会在系统允许的情况下尽量取最大值
# 当open_files_limit没有被配置时，比较max_connection*5和ulimit -n的值并取最大值
# 当open_files_limit被配置时，比较open_files_limit和max_connections*5的值并取最大值
open_files_limit=8192
# 用户名
user=mysql
# 错误log记录文件
log-error=error.log
```