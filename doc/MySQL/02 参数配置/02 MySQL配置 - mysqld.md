# 默认设置相关
- character-set-server<br>
【说明】Server字符集。<br>
【示例】`character-set-server=gbk`<br>
【验证】`mysql> show variables like 'character_set_server';`

- default-storage-engine<br>
【说明】默认存储引擎。<br>
【示例】`default-storage-engine=MyISAM`<br>
【验证】`mysql> show variables like 'default_storage_engine';`

- default-time-zone<br>
【说明】服务器时区。<br>
【示例】`default-time-zone=-2:00`<br>
【验证】`mysql> show variables like '%time_zone%';`


# 系统资源相关
- back_log<br>
【说明】监听队列中可以保持的连接数，对于没建立TCP连接的请求放入监听队列，队列大小为`back_log`，如果数据库存在非常多的连接且出现`connection refused`错误，就应该增大`back_log`的值。可以检查操作系统文档获取这个变量的最大值，如果将此值设置得比操作系统限制的值更大将无效果。<br>
【示例】`back_log=999`<br>
【验证】`mysql> show variables like 'back_log';`

- connect-timeout<br>
【说明】在获取连接阶段（authenticate）起作用。获取MySQL连接是多次握手的结果，除了用户名和密码校验外，还有IP->HOST->DNS->IP验证，任何一步都可能因为网络问题导致线程阻塞。为了防止线程浪费在不必要的校验等待上，超过`connect-timeout`的连接请求将会被拒绝。<br>
【示例】`connect-timeout=6`<br>
【验证】`mysql> select @@connect_timeout;`

- interactive-timeout<br>
【单位】秒<br>
【默认】28800，即8小时<br>
【说明】在连接空闲阶段（sleep）起作用，等待关闭连接的时间。`interactive-timeout`针对交互式连接（如mysql客户端连接数据库），`wait_timeout`针对非交互式连接（如JDBC连接数据库）。连接启动时会根据连接的类型确认会话变量`wait_timeout`是继承全局变量`wait_timeout`还是`interactive_timeout`。<br>
【示例】`interactive-timeout=3600`<br>
【验证】`mysql> select @@interactive_timeout;`

- max_allowed_packet<br>
【说明】MySQL服务可以处理的请求包最大大小，大的插入和更新会受此参数限制，如果值设置过小将导致单个记录超过限制后写入数据库失败，且后续记录写入也将失败。修改时需要设为1024的整数倍，此参数在客户端和服务器最好保持一致。<br>
【示例】`max_allowed_packet=32M`<br>
【验证】`mysql> show variables like 'max_allowed_packet';`

- max_connections<br>
【说明】允许的最大连接进程数，如果访问数据库时出现`Too Many Connections`错误，则需要增大`max_connections`的值。<br>
【示例】`max_connections=999`<br>
【验证】`mysql> select @@max_connections;`

- max_connect_errors<br>
【说明】如果某个用户发起连接的error超过此数值，则该用户的下次连接会被阻塞，直至管理员执行`flush hosts`命令或重启MySQL服务，以防止以非法密码等手段攻击。<br>
【示例】`max_connect_errors=119`<br>
【验证】`mysql> select @@max_connect_errors;`

- net_buffer_length<br>
【说明】控制网络缓冲区大小。每个客户端线程都关联1个连接缓冲区（connection buffer）和1个结果集缓冲区（result buffer），这2个缓冲区的初始大小都是由`net_buffer_length`控制，需要时最大可以自动增长到不超过`max_allowed_packet`，每条SQL语句执行完后结果集缓冲区都会自动恢复到`net_buffer_length`指定大小。一般情况下不应该修改`net_buffer_length`的值，如果这个值很小则可以修改为SQL语句预期的长度，如果SQL语句长度超过这个值，连接缓冲区会自动增长，`net_buffer_length`的最大值为`1048576`（1M）。`net_buffer_length`只用于控制连接缓冲区的初始大小，一旦连接缓冲区初始化完毕就不受`net_buffer_length`控制，而是受`max_allowed_packet`控制。`net_buffer_length`控制连接缓冲区的**下限**，`max_allowed_packet`控制连接缓冲区的**上限**。<br>
【示例】`net_buffer_length=65535`<br>
【验证】`mysql> select @@net_buffer_length;`

- net_read_timeout<br>
【说明】在连接繁忙阶段（query）起作用。连接没有处于空闲（sleep）状态，客户端忙于计算或存储数据等任务导致来不及响应，为了保证连接不被浪费在无尽的等待中，MySQL服务端设置的读取数据的超时时间，一旦超时服务端会主动断开连接。此参数仅针对TCP/IP连接有效，只针对在Activity状态下的线程有效。<br>
【示例】`net_read_timeout=10`<br>
【验证】`mysql> select @@net_read_timeout;`

- net_retry_count<br>
【说明】如果读或写一个通信端口终端，MySQL放弃前尝试连接的次数。<br>
【示例】`net_retry_count=15`<br>
【验证】`mysql> select @@net_retry_count;`

- net_write_timeout<br>
【说明】在连接繁忙阶段（query）起作用。连接没有处于空闲（sleep）状态，客户端忙于计算或存储数据等任务导致来不及响应，为了保证连接不被浪费在无尽的等待中，MySQL服务端设置的写入数据的超时时间，一旦超时服务端会主动断开连接。此参数仅针对TCP/IP连接有效，只针对在Activity状态下的线程有效。<br>
【示例】`net_write_timeout=20`<br>
【验证】`mysql> select @@net_write_timeout;`

- ***open_files_limit?***<br>
【说明】MySQL打开的文件描述限制，默认最小1024。如果没有设置`open_files_limit`，取`max_connections*5`和`ulimit -n`之间的大值；如果设置了`open_files_limit`，取`open_files_limit`和`max_connections*5`之间的大值。<br>
【示例】`open_files_limit=9999`<br>
【验证】

- slave-net-timeout<br>
【说明】设置从库多少秒没有收到主库传来的Binary Logs Events后，Slave IO线程重新连接主库。这个参数如果设置太大可能造成数据库延迟，或主从库连接异常不能及时被发现；但设置太小又会造成主库没有数据更新时频繁重连。<br>
【示例】`slave-net-timeout=30`<br>
【验证】`mysql> select @@slave_net_timeout;`

- table_cache<br>
【说明】<br>
【示例】<br>
【验证】

- thread_cache_size<br>
【说明】<br>
【示例】<br>
【验证】

- thread_concurrency<br>
【说明】<br>
【示例】<br>
【验证】

- thread_stack<br>
【说明】<br>
【示例】<br>
【验证】

- wait-timeout<br>
【单位】秒<br>
【默认】28800，即8小时<br>
【说明】在连接空闲阶段（sleep）起作用，等待关闭连接的时间。`wait_timeout`针对非交互式连接（如JDBC连接数据库）。连接启动时会根据连接的类型确认会话变量`wait_timeout`是继承全局变量`wait_timeout`还是`interactive_timeout`。<br>
【示例】`wait-timeout=3600`<br>
【验证】因为mysql客户端连接属于交互式连接，所以使用`mysql> select @@interactive_timeout;`只能查询到`interactive_timeout`，无法验证`wait_timeout`是否生效。


# 查询缓存设置相关
- query_cache_limit<br>
【说明】<br>
【示例】<br>
【验证】

- query_cache_min_res_unit<br>
【说明】<br>
【示例】<br>
【验证】

- query_cache_size<br>
【说明】<br>
【示例】<br>
【验证】


# 日志设置相关选项
- binlog_cache_size<br>
【说明】<br>
【示例】<br>
【验证】

- expire_logs_days<br>
【说明】<br>
【示例】<br>
【验证】

- general_log<br>
【说明】<br>
【示例】<br>
【验证】

- general_log_file<br>
【说明】<br>
【示例】<br>
【验证】

- log-bin<br>
【说明】打开二进制日志功能，在复制（replication）配置中，主服务器（master）必须打开此选项，如果需要从最后的备份中做基于时间点的恢复，也需要此功能。<br>
【示例】`log-bin=mysql-bin`<br>
【验证】`mysql> show variables like 'log_bin';`

- log-bin-index<br>
【说明】二进制的索引文件名。<br>
【示例】`log-bin-index=mysql-bin.index`<br>
【验证】`mysql> show variables like 'log_bin_index';`

- log-error<br>
【说明】错误日志路径<br>
【示例】`log-error=/var/log/mysql/error.log`<br>
【验证】`mysql> show variables like 'log_error';`

- log_long_format<br>
【说明】<br>
【示例】<br>
【验证】

- log_output<br>
【说明】指定慢查询输出格式，默认为`FILE`，可以设为`TABLE`，然后就可以查询`mysql`数据库中的`slow_log`表<br>
【示例】<br>
【验证】

- log-queries-not-using-indexes<br>
【说明】<br>
【示例】<br>
【验证】

- log_slave_updates<br>
【说明】从服务器（slave）是否将复制事件写入自己的二进制日志。<br>
【示例】`log_slave_updates=0`，`1`表示打开此功能，`0`表示关闭此功能。<br>
【验证】`mysql> show variables like 'log_slave_updates';`

- log-slow-slave-statements<br>
【说明】<br>
【示例】<br>
【验证】

- long-query-time<br>
【说明】<br>
【示例】<br>
【验证】

- long-slow-admin-statements<br>
【说明】<br>
【示例】<br>
【验证】

- max_binlog_size<br>
【说明】<br>
【示例】<br>
【验证】

- max_relay_log_size<br>
【说明】<br>
【示例】<br>
【验证】

- min_examined_row_limit<br>
【说明】<br>
【示例】<br>
【验证】

- relay-log<br>
【说明】定义relay_log的位置和名称，默认在`data`目录下，文件名`host_name-relay-bin.nnnnnn`<br>
【示例】`relay-log=relay-log`<br>
【验证】`mysql> show variables like 'relay_log';`

- relay_log_index<br>
【说明】relay-log的索引文件名<br>
【示例】`relay_log_index=relay-log.index`<br>
【验证】`mysql> show variables like 'relay_log_index';`

- relay-log-purge<br>
【说明】<br>
【示例】<br>
【验证】

- replicate-wild-ignore-table<br>
【说明】<br>
【示例】<br>
【验证】

- slave_skip_errors<br>
【说明】<br>
【示例】<br>
【验证】

- slow_query_log<br>
【说明】控制是否开启慢查询日志，慢查询是指消耗了比`long_query_time`定义的更多时间的查询。如果`log_long_format`被打开，那些没有使用索引的查询也会被记录。<br>
【示例】`slow_query_log=0`，`1`表示打开此功能，`0`表示关闭此功能。<br>
【验证】`mysql> show variables like 'slow_query_log';`

- slow_query_log_file<br>
【说明】<br>
【示例】<br>
【验证】


# 临时&堆设置相关
- max_heap_table_size<br>
【说明】独立的内存表允许的最大容量，此选项防止意外创建一个超大内存表导致用尽所有内存资源。<br>
【示例】`max_heap_table_size=32M`<br>
【验证】`mysql> show variables like 'max_heap_table_size';`

- tmp_table_size<br>
【说明】临时表最大大小，如果超过该值则结果放入磁盘中，此限制针对单个表而非表总和。<br>
【示例】`tmp_table_size=32M`<br>
【验证】`mysql> show variables like 'tmp_table_size';`


# InnoDB存储引擎相关
- innodb_additional_mem_pool_size<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_buffer_pool_size<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_data_file_path<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_data_home_dir<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_fast_shutdown<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_file_io_threads<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_file_per_table<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_flush_log_at_trx_commit<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_flush_method<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_force_recovery<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_lock_wait_timeout<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_log_buffer_size<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_log_file_size<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_log_files_in_group<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_log_group_home_dir<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_open_files<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_read_io_threads<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_status_file<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_thread_concurrency<br>
【说明】<br>
【示例】<br>
【验证】

- innodb_write_io_threads<br>
【说明】<br>
【示例】<br>
【验证】

- skip-innodb<br>
【说明】<br>
【示例】<br>
【验证】


# MyISAM存储引擎相关
- bulk_insert_buffer_size<br>
【说明】<br>
【示例】<br>
【验证】

- join_buffer_size<br>
【说明】<br>
【示例】<br>
【验证】

- key_buffer_size<br>
【说明】<br>
【示例】<br>
【验证】

- myisam_max_sort_file_size<br>
【说明】<br>
【示例】<br>
【验证】

- myisam_recover<br>
【说明】<br>
【示例】<br>
【验证】

- myisam_repair_threads<br>
【说明】<br>
【示例】<br>
【验证】

- myisam_sort_buffer_size<br>
【说明】<br>
【示例】<br>
【验证】

- read_buffer_size<br>
【说明】<br>
【示例】<br>
【验证】

- read_rnd_buffer_size<br>
【说明】<br>
【示例】<br>
【验证】

- sort_buffer_size<br>
【说明】<br>
【示例】<br>
【验证】

