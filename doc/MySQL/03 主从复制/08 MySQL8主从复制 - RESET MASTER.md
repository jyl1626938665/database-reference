执行`RESET MASTER`前查看Master状态
```
mysql> show master status\G;
*************************** 1. row ***************************
             File: binlog.000006
         Position: 1752
     Binlog_Do_DB: 
 Binlog_Ignore_DB: 
Executed_Gtid_Set: 
1 row in set (0.00 sec)
```

执行`RESET MASTER`后查看Master状态
```
mysql> show master status\G;
*************************** 1. row ***************************
             File: binlog.000001
         Position: 156
     Binlog_Do_DB: 
 Binlog_Ignore_DB: 
Executed_Gtid_Set: 
1 row in set (0.00 sec)
```

从对比可以看出所有的binlog都已被删除，binlog从`000001`开始记录。

***注意事项***<br>
当数据库需要清理binlog文件时可以通过操作系统进行删除，也可以执行`RESET MASTER`删除。**但是，如果当前操作的是主数据库，且主从数据库正常时，不能使用此方式删除**。

***使用场景***<br>
第一次搭建主从数据库时用于主库的初始化binlog操作。