# 1 配置第一个主数据库A
## 1.1 配置
```
[mysqld]
# 服务器唯一ID
server-id=1
# 启用二进制日志
log-bin=mysql-bin
# 设置不要复制的数据库
binlog-ignore-db=information_schema
binlog-ignore-db=mysql
binlog-ignore-db=performance_schema
binlog-ignore-db=sys
# 设置需要复制的数据库
binlog-do-db=test
# 设置格式
binlog_format=mixed
relay-log=relay-bin
relay-log-index=slave-relay-bin.index
```
执行`service mysql restart`命令重启MySQL服务。

## 1.2 创建用户slave并授权
```
mysql> create user 'slave'@'%' identified with mysql_native_password by '123456';
Query OK, 0 rows affected (0.01 sec)

mysql> grant replication slave on *.* to 'slave'@'%';
Query OK, 0 rows affected (0.00 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)
```

## 1.3 查看主数据库状态
```
mysql> show master status;
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                | Executed_Gtid_Set |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| mysql-bin.000008 |      156 | test         | information_schema,mysql,performance_schema,sys |                   |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
1 row in set (0.00 sec)
```


# 2 配置第二个主数据库B
## 2.1 配置
```
[mysqld]
# 服务器唯一ID
server-id=2
# 启用二进制日志
log-bin=mysql-bin
# 设置不要复制的数据库
binlog-ignore-db=information_schema
binlog-ignore-db=mysql
binlog-ignore-db=performance_schema
binlog-ignore-db=sys
# 设置需要复制的数据库
binlog-do-db=test
# 设置格式
binlog_format=mixed
relay-log=relay-bin
relay-log-index=slave-relay-bin.index
```
执行`service mysql restart`命令重启MySQL服务。除`server-id`和`auto-increment-offset`外，其它配置与第一个主数据库相同。

## 2.2 创建用户slave并授权
```
mysql> create user 'slave'@'%' identified with mysql_native_password by '123456';
Query OK, 0 rows affected (0.01 sec)

mysql> grant replication slave on *.* to 'slave'@'%';
Query OK, 0 rows affected (0.00 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)
```

## 2.3 查看主数据库状态
```
mysql> show master status;
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                | Executed_Gtid_Set |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| mysql-bin.000006 |      156 | test         | information_schema,mysql,performance_schema,sys |                   |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
1 row in set (0.00 sec)
```


# 3 配置从数据库
## 3.1 配置
```
[mysqld]
server-id=9
relay-log=mysql-relay
```

## 3.2 设置主数据库
```
mysql> change master to master_host='MASTER_A_IP', master_port=3306, master_user='slave', master_password='111111', master_log_file='mysql-bin.000008', master_log_pos=156 for channel '100';
Query OK, 0 rows affected, 9 warnings (0.00 sec)

mysql> change master to master_host='MASTER_B_IP', master_port=3306, master_user='slave', master_password='222222', master_log_file='mysql-bin.000006', master_log_pos=156 for channel '200';
Query OK, 0 rows affected, 9 warnings (0.00 sec)
```

## 3.3 重启slave
```
mysql> stop slave;
Query OK, 0 rows affected, 3 warnings (0.00 sec)

mysql> start slave;
Query OK, 0 rows affected, 1 warning (0.01 sec)
```


# 4 查看从库状态，Slave_IO_Running和Slave_SQL_Running都为Yes时说明配置成功
```
mysql> show slave status\G;
*************************** 1. row ***************************
...
            Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
...
*************************** 2. row ***************************
...
            Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
...
```


# 注意事项
本文示例中两个主数据库间并无同步关系，从数据库只是对两个主数据库数据的备份同步，即主数据库A中的更改会同步到从数据库中，但不会同步到主数据库B中，同理，主数据库B中的更改会同步到从数据库中，也不会同步到主数据库A中。如果配置主数据库A、B间的同步关系，请参考：[MySQL8主从复制 - 双主](https://gitee.com/jyl1626938665/database-reference/blob/master/doc/MySQL/205%20MySQL8%E4%B8%BB%E4%BB%8E%E5%A4%8D%E5%88%B6%20-%20%E5%8F%8C%E4%B8%BB.md)