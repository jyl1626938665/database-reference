# 双主架构方案
- 两台MySQL都可读写且互为主备，默认一台（MasterA）负责数据写入，另一台（MasterB）备用；
- MasterA和MasterB互为主从；
- 两台主库之间做高可用，可以采用keepalived方案；
- 所有提供服务的从服务器与MasterB进行主从同步（双主多从）；
- 建议采用高可用策略的时候，MasterA或MasterB均不因宕机回复后而抢占VIP（非抢占模式）；
- 双主架构需要考虑自增长ID问题，需要特别设置配置文件，如双主可以使用奇偶。

# 双主架构优点
- 将读写请求分摊到两个主节点，有效提升服务器使用率；
- 主节点发生故障后可以快速进行主从切换；
- 故障节点恢复后可以通过复制进行数据恢复（应用其它节点数据）和数据同步（将未同步数据发送给其它节点）。

# 双主架构缺点
- 主节点上实例发生故障后可能会存在部分数据（binlog）未同步到另外的主节点，从而导致数据丢失（直至故障节点恢复）；
- 双主模式下很容易因数据访问控制不当导致数据冲突；
- 为提高系统高可用性，双主架构会被扩展成双主多从结构，同样存在主节点发生故障后多个从库选主和恢复复制的问题。

# MMM架构
双主架构下MySQL本身没有自动故障切换能力，Google开源项目MySQL-MMM（Master-Master Replication Manager for MySQL）能够用来监控MySQL主主复制并实现自动故障转移。<br>
在使用`after_sync`的半同步复制的双主架构下，能有效确保两个主库的`binlog`同步，再配合MMM工具实现自动故障转移，确保集群高可用。

# 配置过程
1 在配置文件中设置配置项
- MasterA
```
[mysqld]
default_authentication_plugin=mysql_native_password

# 服务器唯一ID
server-id=1
# 启用二进制日志
log-bin=mysql-bin
# 设置不要复制的数据库
binlog-ignore-db=information_schema
binlog-ignore-db=mysql
binlog-ignore-db=performance_schema
binlog-ignore-db=sys
# 设置格式
binlog_format=mixed
relay-log=relay-bin
relay-log-index=slave-relay-bin.index
auto-increment-increment=2
auto-increment-offset=1
log-slave-updates
```

- MasterB
```
[mysqld]
default_authentication_plugin=mysql_native_password

# 服务器唯一ID
server-id=2
# 启用二进制日志
log-bin=mysql-bin
# 设置不要复制的数据库
binlog-ignore-db=information_schema
binlog-ignore-db=mysql
binlog-ignore-db=performance_schema
binlog-ignore-db=sys
# 设置格式
binlog_format=mixed
relay-log=relay-bin
relay-log-index=slave-relay-bin.index
auto-increment-increment=2
auto-increment-offset=2
log-slave-updates
```

注意：MasterA和MasterB配置文件差异在`server-id`和`auto-increment-offset`。

2 创建用户并授权远程访问
- MasterA
```
mysql> create user 'slave'@'%' identified with mysql_native_password by '111111';
Query OK, 0 rows affected (0.02 sec)

mysql> grant replication slave on *.* to 'slave'@'%';
Query OK, 0 rows affected (0.01 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)
```

- MasterB
```
mysql> create user 'slave'@'%' identified with mysql_native_password by '222222';
Query OK, 0 rows affected (0.04 sec)

mysql> grant replication slave on *.* to 'slave'@'%';
Query OK, 0 rows affected (0.01 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)
```

3 查看master状态
- MasterA
```
mysql> show master status;
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                | Executed_Gtid_Set |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| mysql-bin.000004 |      843 |              | information_schema,mysql,performance_schema,sys |                   |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
```

- MasterB
```
mysql> show master status;
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                | Executed_Gtid_Set |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| mysql-bin.000001 |      843 |              | information_schema,mysql,performance_schema,sys |                   |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
```

4 配置同步
- MasterA
```
mysql> change master to master_host='MasterB IP',master_port=3306,master_user='slave',master_password='222222',master_log_file='mysql-bin.000001',master_log_pos=843;
Query OK, 0 rows affected, 9 warnings (0.09 sec)

mysql> start slave;
Query OK, 0 rows affected, 1 warning (0.03 sec)
```

- MasterB
```
mysql> change master to master_host='MasterA IP',master_port=3306,master_user='slave',master_password='111111',master_log_file='mysql-bin.000004',master_log_pos=843;
Query OK, 0 rows affected, 9 warnings (0.11 sec)

mysql> start slave;
Query OK, 0 rows affected, 1 warning (0.02 sec)
```

5 检查是否生效
在MasterA和MasterB上各执行`mysql> show slave status\G;`，如果显示的`Slave_IO_Running`和`Slave_SQL_Running`的值都为`Yes`则说明同步生效。
