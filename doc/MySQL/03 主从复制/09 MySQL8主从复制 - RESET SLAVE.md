`RESET SLAVE`命令使SLAVE忘记主从复制关系的位置信息，删除`master.info`和`relay-log.info`文件以及所有的relay log文件，并重新启用一个新的relay log文件。

***注意事项***<br>
执行`RESET SLAVE`前必须先执行`STOP SLAVE`。

***使用场景***<br>
当原有的主从关系被破坏之后，从库经过重新初始化后直接连接会报`ERROR 1201`错误，执行`RESET SLAVE`后重新配置主从连接就可以了。<br>
如果是需要删除mysql binlog和relay log，那么通过操作系统的删除或`PURGE`命令都可以，但是涉及到MySQL主从配置场景便需要使用`RESET MASTER`和`RESET SLAVE`来解决。

