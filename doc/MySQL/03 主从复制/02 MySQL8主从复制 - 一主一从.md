# 1 配置主数据库
```
[mysqld]
character-set-server=utf8
lower-case-table-names=1
default_authentication_plugin=mysql_native_password

# 主从复制-主机配置
# 主服务器唯一ID
server-id=1
# 启用二进制日志
log-bin=mysql-bin
# 设置不要复制的数据库
binlog-ignore-db=information_schema
binlog-ignore-db=mysql
binlog-ignore-db=performance_schema
binlog-ignore-db=sys
# 设置需要复制的数据库
binlog-do-db=test
# 设置格式
binlog_format=STATEMENT
```
执行`service mysql restart`命令重启MySQL服务。

# 2 配置从数据库
```
[mysqld]
character-set-server=utf8
lower-case-table-names=1
default_authentication_plugin=mysql_native_password

# 主从复制-从机配置
# 从服务器唯一ID
server-id=2
# 启用中继日志
relay-log=mysql-relay
```
执行`service mysql restart`命令重启MySQL服务。

# 3 主数据库创建用户slave并授权
```
mysql> create user 'slave'@'%' identified with mysql_native_password by '123456';
Query OK, 0 rows affected (0.01 sec)

mysql> grant replication slave on *.* to 'slave'@'%';
Query OK, 0 rows affected (0.00 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)
```

# 4 从数据库验证slave用户是否可用
```
mysql -h主机IP -uslave -p123456
```

# 5 主数据库查询服务ID及Master状态
```
mysql> show variables like 'server_id';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| server_id     | 1     |
+---------------+-------+
1 row in set (0.00 sec)

mysql> show master status;
+---------------+----------+--------------+------------------+-------------------+
| File          | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+---------------+----------+--------------+------------------+-------------------+
| binlog.000005 |      835 |              |                  |                   |
+---------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)
```

# 6 从数据库中设置主数据库
```
mysql> show variables like 'server_id';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| server_id     | 2     |
+---------------+-------+
1 row in set (0.00 sec)

mysql> change master to master_host='MASTER_IP', master_port=3306, master_user='slave', master_password='123456', master_log_file='binlog.000005', master_log_pos=835;
Query OK, 0 rows affected, 9 warnings (0.10 sec)

mysql> start slave;
Query OK, 0 rows affected, 1 warning (0.02 sec)
```

# 7 查看从库状态，Slave_IO_Running和Slave_SQL_Running都为Yes时说明配置成功
```
mysql> show slave status\G;
*************************** 1. row ***************************
...
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
...
```

# 注意事项
**如果主数据库`my.cnf`中配置了`binlog-do-db`（需要复制的数据库），则此数据库在主库和从库中都要存在，否则会报错（Worker 1 failed executing transaction ‘ANONYMOUS’ at master log），且查看从库状态（`show slave status\G`）会发现`Slave_SQL_Running`变成`No`。**

