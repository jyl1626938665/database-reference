准备4个MySQL服务：A/B/C/D，其中A/B为主数据库，C/D为从数据库。


# 1 配置主数据库
## 1.1 配置
- 主数据库A
```
[mysqld]
# 主从复制-主机配置
# 主服务器唯一ID
server-id=1
# 启用二进制日志
log-bin=mysql-bin
# 设置不要复制的数据库
binlog-ignore-db=information_schema
binlog-ignore-db=mysql
binlog-ignore-db=performance_schema
binlog-ignore-db=sys
# 设置主键自增步长为2
auto-increment-increment=2
# 设置主键从1开始
auto-increment-offset=1
log-slave-updates
binlog_format=STATEMENT
```

- 主数据库B
```
[mysqld]
# 主从复制-主机配置
# 主服务器唯一ID
server-id=2
# 启用二进制日志
log-bin=mysql-bin
# 设置不要复制的数据库
binlog-ignore-db=information_schema
binlog-ignore-db=mysql
binlog-ignore-db=performance_schema
binlog-ignore-db=sys
# 设置主键自增步长为2
auto-increment-increment=2
# 设置主键从2开始
auto-increment-offset=2
log-slave-updates
binlog_format=STATEMENT
```

## 1.2 重启两个主数据库的MySQL服务
```
service mysql restart
```

## 1.3 创建拥有远程访问权限的用户
- 主数据库A
```
mysql> create user 'slave'@'%' identified with mysql_native_password by '111111';
Query OK, 0 rows affected (0.02 sec)

mysql> grant replication slave on *.* to 'slave'@'%';
Query OK, 0 rows affected (0.01 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)
```

- 主数据库B
```
mysql> create user 'slave'@'%' identified with mysql_native_password by '222222';
Query OK, 0 rows affected (0.02 sec)

mysql> grant replication slave on *.* to 'slave'@'%';
Query OK, 0 rows affected (0.01 sec)

mysql> flush privileges;
Query OK, 0 rows affected (0.00 sec)
```

## 1.4 重置两个主数据库的偏移量
```
mysql> reset master;
```

## 1.5 查看master状态
- 主数据库A
```
mysql> show master status;
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                | Executed_Gtid_Set |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| mysql-bin.000001 |      156 |              | information_schema,mysql,performance_schema,sys |                   |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
1 row in set (0.00 sec)
```

- 主数据库B
```
mysql> show master status;
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB                                | Executed_Gtid_Set |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
| mysql-bin.000001 |      156 |              | information_schema,mysql,performance_schema,sys |                   |
+------------------+----------+--------------+-------------------------------------------------+-------------------+
1 row in set (0.00 sec)
```

## 1.6 设置2个主数据库互为主从
- 主数据库A
```
mysql> stop slave;
Query OK, 0 rows affected, 2 warnings (0.00 sec)

mysql> change master to master_host='MASTER_B_IP', master_port=3306, master_user='slave', master_password='222222', master_log_file='mysql-bin.000001', master_log_pos=156;
Query OK, 0 rows affected, 9 warnings (0.01 sec)

mysql> start slave;
Query OK, 0 rows affected, 1 warning (0.01 sec)
```

- 主数据库B
```
mysql> stop slave;
Query OK, 0 rows affected, 2 warnings (0.01 sec)

mysql> change master to master_host='MASTER_A_IP', master_port=3306, master_user='slave', master_password='111111', master_log_file='mysql-bin.000001', master_log_pos=156;
Query OK, 0 rows affected, 9 warnings (0.10 sec)

mysql> start slave;
Query OK, 0 rows affected, 1 warning (0.04 sec)
```

## 1.7 查看2个主数据库的slave状态
- 主数据库A
```
mysql> show slave status\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for source to send event
                  Master_Host: MASTER_B_IP
                  Master_User: slave
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000001
          Read_Master_Log_Pos: 156
               Relay_Log_File: iot-ubuntu20-relay-bin.000002
                Relay_Log_Pos: 324
        Relay_Master_Log_File: mysql-bin.000001
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
......
```

- 主数据库B
```
mysql> show slave status\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for source to send event
                  Master_Host: MASTER_A_IP
                  Master_User: slave
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000001
          Read_Master_Log_Pos: 156
               Relay_Log_File: ubuntu18-relay-bin.000002
                Relay_Log_Pos: 324
        Relay_Master_Log_File: mysql-bin.000001
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes

```
`Slave_IO_Running`和`Slave_SQL_Running`的值都为`Yes`说明配置成功。


# 2 配置从数据库
## 2.1 配置
- 从数据库C
```
# 服务器唯一ID
server-id=3
# 启用中继日志
relay-log=mysql-relay
```

- 从数据库D
```
# 服务器唯一ID
server-id=4
# 启用中继日志
relay-log=mysql-relay
```

## 2.2 重启两个从数据库的MySQL服务
```
service mysql restart
```

## 2.3 从数据库中设置主数据库
- 从数据库C
```
mysql> reset slave;
Query OK, 0 rows affected, 1 warning (0.07 sec)

mysql> stop slave;
Query OK, 0 rows affected, 2 warnings (0.00 sec)

mysql> change master to master_host='MASTER_A_IP', master_port=3306, master_user='slave', master_password='111111', master_log_file='mysql-bin.000001', master_log_pos=156;
Query OK, 0 rows affected, 9 warnings (0.08 sec)

mysql> start slave;
Query OK, 0 rows affected, 1 warning (0.02 sec)
```

- 从数据库D
```
mysql> reset slave;
Query OK, 0 rows affected, 1 warning (0.09 sec)

mysql> stop slave;
Query OK, 0 rows affected, 2 warnings (0.00 sec)

mysql> change master to master_host='MASTER_B_IP', master_port=3306, master_user='slave', master_password='222222', master_log_file='mysql-bin.000001', master_log_pos=156;
Query OK, 0 rows affected, 9 warnings (0.09 sec)

mysql> start slave;
Query OK, 0 rows affected, 1 warning (0.03 sec)
```

## 2.4 查看2个从数据库的slave状态
- 从数据库C
```
mysql> show slave status\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for source to send event
                  Master_Host: MASTER_A_IP
                  Master_User: slave
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000001
          Read_Master_Log_Pos: 156
               Relay_Log_File: mysql-relay.000002
                Relay_Log_Pos: 324
        Relay_Master_Log_File: mysql-bin.000001
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
......
```

- 从数据库D
```
mysql> show slave status\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for source to send event
                  Master_Host: MASTER_B_IP
                  Master_User: slave
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000001
          Read_Master_Log_Pos: 156
               Relay_Log_File: mysql-relay.000002
                Relay_Log_Pos: 324
        Relay_Master_Log_File: mysql-bin.000001
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
......
```
`Slave_IO_Running`和`Slave_SQL_Running`的值都为`Yes`说明配置成功。

至此，MySQL双主双从配置完成，可以通过创建数据库、表和添加、修改、删除数据进行测试。