```
mysql> stop slave;
Query OK, 0 rows affected, 2 warnings (0.00 sec)

mysql> reset slave;
Query OK, 0 rows affected, 1 warning (0.07 sec)

mysql> change master to master_user='', master_host=' ', master_password='';
Query OK, 0 rows affected, 6 warnings (0.10 sec)
```
**【注意】**`master_host`即使为空也应该使用空格代替，不能什么都不写，否则会报以下错误
```
ERROR 1210 (HY000): Incorrect arguments to MASTER_HOST
```