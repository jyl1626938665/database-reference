# 问题描述
`/etc/my.cnf`中的配置不生效，无论重启MySQL服务还是重启操作系统都不生效。


# 版本环境
- MySQL版本：8.0.27
- 操作系统版本：Ubuntu 18.04、Ubuntu 20.04


# 排查过程
1 查找系统中全部`my.cnf`文件。
```
root@...:~# locate my.cnf
/etc/my.cnf
/etc/alternatives/my.cnf
/etc/mysql/my.cnf
/etc/mysql/my.cnf.fallback
/var/lib/dpkg/alternatives/my.cnf
```

2 查找mysql进程是否指定加载哪个配置文件。
```
root@...:~# ps aux|grep mysql|grep 'my.cnf'
```
如果无任何输出，表示没有指定配置文件。

3 查看mysql默认读取的`my.cnf`。
```
root@...:~# mysql --help|grep 'my.cnf'
                      order of preference, my.cnf, $MYSQL_TCP_PORT,
/etc/my.cnf /etc/mysql/my.cnf ~/.my.cnf
root@...:~# mysqld --verbose --help | grep -A 1 'Default options'
Default options are read from the following files in the given order:
/etc/my.cnf /etc/mysql/my.cnf ~/.my.cnf
```
可见mysql默认会搜索`/etc/my.cnf`、`/etc/mysql/my.cnf`、`~/.my.cnf`，顺序排前的优先。

4 实际上`/etc/my.cnf`存在但配置不生效，查看`/etc/mysql/my.cnf`，实际内容只有两行：
```
!includedir /etc/mysql/conf.d/
!includedir /etc/mysql/mysql.conf.d/
```
- `!includedir /etc/mysql/conf.d/`：表示包含`/etc/mysql/conf.d/`这个路径下的配置文件，配置文件必须以`.cnf`为后缀。
- `!includedir /etc/mysql/mysql.conf.d/`：表示包含`/etc/mysql/mysql.conf.d/`这个路径下的配置文件，配置文件必须以`.cnf`为后缀。

5 查看`~/.my.cnf`，此文件是个人用户配置文件，需要人为添加配置信息，如果没有添加则为空。

6 查看第4步中包含的两个路径下有哪些配置文件。
```
root@...:/etc# cd /etc/mysql/conf.d/
root@...:/etc/mysql/conf.d# ll
total 12
drwxr-xr-x 2 root root 4096 Mar 31 17:09 ./
drwxr-xr-x 4 root root 4096 Mar 31 18:55 ../
-rw-r--r-- 1 root root 1273 Sep 28  2021 mysql.cnf
root@...:/etc/mysql/conf.d# cd /etc/mysql/mysql.conf.d/
root@...:/etc/mysql/mysql.conf.d# ll
total 12
drwxr-xr-x 2 root root 4096 Mar 31 17:33 ./
drwxr-xr-x 4 root root 4096 Mar 31 18:55 ../
-rw-r--r-- 1 root root 1413 Mar 31 17:33 mysqld.cnf
```
`/etc/mysql/conf.d/mysql.cnf`文件内容只有1行：
```
[mysql]
```
`/etc/mysql/mysql.conf.d/mysqld.cnf`文件内容如下：
```
[mysqld]
pid-file        = /var/run/mysqld/mysqld.pid
socket          = /var/run/mysqld/mysqld.sock
datadir         = /var/lib/mysql
log-error       = /var/log/mysql/error.log
```

7 修改`/etc/mysql/mysql.conf.d/mysqld.cnf`文件，添加以下配置项：
```
max_connections=996
```
从起MySQL服务器后登录MySQL服务器查询配置是否已生效：
```
mysql> select @@max_connections;
+-------------------+
| @@max_connections |
+-------------------+
|               996 |
+-------------------+
1 row in set (0.00 sec)
```
可见`/etc/mysql/mysql.conf.d/mysqld.cnf`中配置项修改可以生效，但是`/etc/my.cnf`中配置项修改不生效。 


# 根因确认
Linux内核中有一个强制访问控制系统AppArmor（Application Armor），AppArmor允许系统管理员将每个程序与一个安全配置文件关联，从而限制程序的功能。简单来说，AppArmor是与SELinux类似的一个访问控制系统，通过它可以指定程序读、写或运行哪些文件，是否可以打开网络端口等。作为对传统Unix的自主访问控制模块的补充，AppArmor提供了强制访问控制机制，已经被整合到2.6版本的Linux内核中。Ubuntu自带AppArmor，`/etc/my.cnf`不生效是应为AppArmor限制了mysql服务的文件访问范围。

1 查看服务器上配置
```
root@...:~# vi /etc/apparmor.d/usr.sbin.mysqld
...
# Allow config access
  /etc/mysql/** r,
...
```
可见mysql并没有`/etc/my.cnf`的访问权限。

2 修改`/etc/apparmor.d/usr.sbin.mysqld`，添加`/etc/my.cnf`访问权限
```
# Allow config access
  /etc/mysql/** r,
  /etc/*.cnf rw,
```

3 重启操作系统，命令：`reboot`

4 在`/etc/my.cnf`中添加任意配置，重启MySQL服务器后检查配置是否已生效。
