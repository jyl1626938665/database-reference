1 登录MySQL，命令：` mysql -uroot -p`

2 访问`mysql`数据库
```
mysql> use mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
```

3 查看`root`用户相关信息
```
mysql> select host, user from user;
+-----------+------------------+
| host      | user             |
+-----------+------------------+
| localhost | mysql.infoschema |
| localhost | mysql.session    |
| localhost | mysql.sys        |
| localhost | root             |
+-----------+------------------+
4 rows in set (0.00 sec)
```
`root`用户的`host`为`localhost`，说明只支持本地访问，不支持远程访问。

4 更改`host`默认配置
```
mysql> update user set host='%' where user='root';
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0
```
5 执行刷新权限命令：`flush privileges;`。