# 1 查询已安装的MySQL依赖
```
dpkg --list|grep mysql
```

# 2 先卸载`mysql-common`
```
sudo apt-get remove mysql-common
```

# 3 执行卸载命令
```
sudo apt-get autoremove --purge mysql-server-8.0
```

# 4 清除残留数据
```
dpkg -l|grep ^rc|awk '{print$2}'|sudo xargs dpkg -P
```

# 5 再次查看剩余的MySQL依赖
```
dpkg --list|grep mysql
```
依次执行`sudo apt-get autoremove --purge XXX`卸载直至无剩余MySQL依赖。