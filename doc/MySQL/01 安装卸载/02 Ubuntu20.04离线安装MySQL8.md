# 1 下载离线安装包
本示例使用：`mysql-server_8.0.24-1ubuntu20.04_amd64.deb-bundle.tar`


# 2 下载依赖包
- libmecab2：http://archive.ubuntu.com/ubuntu/pool/main/m/mecab/libmecab2_0.996-10build1_amd64.deb


# 3 上传到服务器指定目录解压
解压命令：`tar -xvf mysql-server_8.0.24-1ubuntu20.04_amd64.deb-bundle.tar`


# 4 按照以下顺序安装
## 4.1 mysql-community-client-plugins
```
root@...:/opt/mysql# dpkg -i mysql-community-client-plugins_8.0.27-1ubuntu20.04_amd64.deb
Selecting previously unselected package mysql-community-client-plugins.
(Reading database ... 70869 files and directories currently installed.)
Preparing to unpack mysql-community-client-plugins_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking mysql-community-client-plugins (8.0.27-1ubuntu20.04) ...
Setting up mysql-community-client-plugins (8.0.27-1ubuntu20.04) ...
Processing triggers for libc-bin (2.31-0ubuntu9) ...
```

## 4.2 mysql-community-client-core
```
root@...:/opt/mysql# dpkg -i mysql-community-client-core_8.0.27-1ubuntu20.04_amd64.deb
Selecting previously unselected package mysql-community-client-core.
(Reading database ... 70883 files and directories currently installed.)
Preparing to unpack mysql-community-client-core_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking mysql-community-client-core (8.0.27-1ubuntu20.04) ...
Setting up mysql-community-client-core (8.0.27-1ubuntu20.04) ...
Processing triggers for man-db (2.9.1-1) ...
```

## 4.3 mysql-common
```
root@...:/opt/mysql# dpkg -i mysql-common_8.0.27-1ubuntu20.04_amd64.deb
Selecting previously unselected package mysql-common.
(Reading database ... 70895 files and directories currently installed.)
Preparing to unpack mysql-common_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking mysql-common (8.0.27-1ubuntu20.04) ...
Setting up mysql-common (8.0.27-1ubuntu20.04) ...
update-alternatives: using /etc/mysql/my.cnf.fallback to provide /etc/mysql/my.cnf (my.cnf) in auto mode
```

## 4.4 mysql-community-client
```
root@...:/opt/mysql# dpkg -i mysql-community-client_8.0.27-1ubuntu20.04_amd64.deb
Selecting previously unselected package mysql-community-client.
(Reading database ... 70909 files and directories currently installed.)
Preparing to unpack mysql-community-client_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking mysql-community-client (8.0.27-1ubuntu20.04) ...
Setting up mysql-community-client (8.0.27-1ubuntu20.04) ...
Processing triggers for man-db (2.9.1-1) ...
```

## 4.5 libmysqlclient21
```
root@...:/opt/mysql# dpkg -i libmysqlclient21_8.0.27-1ubuntu20.04_amd64.deb
Selecting previously unselected package libmysqlclient21:amd64.
(Reading database ... 70958 files and directories currently installed.)
Preparing to unpack libmysqlclient21_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking libmysqlclient21:amd64 (8.0.27-1ubuntu20.04) ...
Setting up libmysqlclient21:amd64 (8.0.27-1ubuntu20.04) ...
Processing triggers for libc-bin (2.31-0ubuntu9) ...
```

## 4.6 libmysqlclient-dev
```
root@...:/opt/mysql# dpkg -i libmysqlclient-dev_8.0.27-1ubuntu20.04_amd64.deb
Selecting previously unselected package libmysqlclient-dev.
(Reading database ... 70966 files and directories currently installed.)
Preparing to unpack libmysqlclient-dev_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking libmysqlclient-dev (8.0.27-1ubuntu20.04) ...
Setting up libmysqlclient-dev (8.0.27-1ubuntu20.04) ...
Processing triggers for man-db (2.9.1-1) ...
```

## 4.7 mysql-client
```
root@...:/opt/mysql# dpkg -i mysql-client_8.0.27-1ubuntu20.04_amd64.deb
Selecting previously unselected package mysql-client.
(Reading database ... 70999 files and directories currently installed.)
Preparing to unpack mysql-client_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking mysql-client (8.0.27-1ubuntu20.04) ...
Setting up mysql-client (8.0.27-1ubuntu20.04) ...
```

## 4.8 libmecab2
```
root@...:/opt/mysql# dpkg -i libmecab2_0.996-10build1_amd64.deb
Selecting previously unselected package libmecab2:amd64.
(Reading database ... 71130 files and directories currently installed.)
Preparing to unpack libmecab2_0.996-10build1_amd64.deb ...
Unpacking libmecab2:amd64 (0.996-10build1) ...
Setting up libmecab2:amd64 (0.996-10build1) ...
Processing triggers for libc-bin (2.31-0ubuntu9) ...
```

## 4.9 mysql-community-server-core
```
root@...:/opt/mysql# dpkg -i mysql-community-server-core_8.0.27-1ubuntu20.04_amd64.deb
(Reading database ... 71136 files and directories currently installed.)
Preparing to unpack mysql-community-server-core_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking mysql-community-server-core (8.0.27-1ubuntu20.04) over (8.0.27-1ubuntu20.04) ...
Setting up mysql-community-server-core (8.0.27-1ubuntu20.04) ...
Processing triggers for libc-bin (2.31-0ubuntu9) ...
Processing triggers for man-db (2.9.1-1) ...
```

## 4.10 mysql-community-server
```
root@...:/opt/mysql# dpkg -i mysql-community-server_8.0.27-1ubuntu20.04_amd64.deb
Selecting previously unselected package mysql-community-server.
(Reading database ... 71136 files and directories currently installed.)
Preparing to unpack mysql-community-server_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking mysql-community-server (8.0.27-1ubuntu20.04) ...
Setting up mysql-community-server (8.0.27-1ubuntu20.04) ...
update-alternatives: using /etc/mysql/mysql.cnf to provide /etc/mysql/my.cnf (my.cnf) in auto mode
Created symlink /etc/systemd/system/multi-user.target.wants/mysql.service → /lib/systemd/system/mysql.service.
```

## 4.11 mysql-server
```
root@...:/opt/mysql# dpkg -i mysql-server_8.0.27-1ubuntu20.04_amd64.deb
Selecting previously unselected package mysql-server.
(Reading database ... 71155 files and directories currently installed.)
Preparing to unpack mysql-server_8.0.27-1ubuntu20.04_amd64.deb ...
Unpacking mysql-server (8.0.27-1ubuntu20.04) ...
Setting up mysql-server (8.0.27-1ubuntu20.04) ...
```

# 5 重启MySQL服务
命令：`sudo service mysql restart`


# 附
## 国内镜像地址
- [中国科学技术大学](http://mirrors.ustc.edu.cn/mysql-ftp/Downloads/)
- [搜狐](http://mirrors.sohu.com/mysql/)